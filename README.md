# Node Chat

(Iniciar docker - Solo necesario en Fedora)

sudo mkdir /sys/fs/cgroup/systemd

sudo mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd 

(Abrir terminal)

docker stop $(docker ps -a -q)

docker rm $(docker ps -a -q)

docker system prune -a 

docker-compose up -d

(Abrir la extensión docker y attach shell en node alpine)

npm install 

npm run start

Abrir localhost:4010

(En caso de que la database de error, borrar todo el contenido de la carpeta "Data" y hacer un nuevo docker compose)
