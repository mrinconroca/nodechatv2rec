//Aplicacion especifico para API
require("dotenv").config();
var express = require("express");
var api = express();
var server = require("http").createServer(api);
var mongoose = require("mongoose");
var io = require("socket.io")(server)
var numUsers = 0;

server.listen(process.env.API_PORT,(err,res)=>{
    if(err) console.log("Error en el servidor"+err);
    console.log("API conectado");
})

mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true},
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database: ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

var bodyParser = require('body-parser');

api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: false }));

api.use(express.static(__dirname + "/public"));
api.set("views", __dirname + "/views");
api.set("view engine", "pug");

var apiRoutes = require(__dirname+'/app/routes/api');

api.route('/').get((req, res, next) => {
    res.redirect("/api/chat")
})

api.use("/api",apiRoutes);

io.on("connection", (socket) => {
    console.log("Socket conectado");
    socket.user = "Usuario " + numUsers++;
    socket.join(socket.user);
    //Permite conexion con todos
    socket.on("newMSG", (data) => {
        data["user"] = socket.user;
        console.log(data);
        if (data["room"] == "all") {
            socket.broadcast.emit("newMSG", data);
        } else if (data["room"] == "1") {
            socket.broadcast.emit("newMSG", data);
        } else {
            socket.to(data["room"]).emit("newMSG", data);
        }
    })
    socket.on("newUser", (data) => {
        data["user"] = socket.user;
        socket.broadcast.emit("newUser", data);
    })
})