var moongose = require("mongoose"),
    Formulario = require("../models/Formulario");

exports.load=async()=>{
    var res= await Formulario.find({})
    return res;
}

exports.loadType=async(nombre)=>{
    var res= await Formulario.find({"nombre": nombre})
    return res;
}

exports.loadTypeEst=async(estado)=>{
    var res= await Formulario.find({"estado": estado})
    return res;
}

exports.save = (req)=>{
    var newForm = new Formulario(req);
    newForm.save((err,res)=>{
        if(err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}

exports.saveCont = (req)=>{
    var newForm = new Formulario(req);
    newForm.save((err,res)=>{
        if(err) console.log(err);
        console.log("Test en la BD");
        return res;
    });
}

exports.incidencia_delete = function (id) {
    var incDel = {
        _id: id,
    };
    Formulario.deleteOne(incDel, function (err) {
        if (err) return handleError(err);
    });
};

exports.incidencia_update = function (id, newIncidencia, newUrgencia, newNombre, newDni, newDescripcion, newEstado) {
    var incidenciaToUpdate = {
        _id: id,
    };
    var newValues = {
        $set: {
            incidencia: newIncidencia,
            urgencia: newUrgencia,
            nombre: newNombre,
            dni: newDni,
            descripcion: newDescripcion,
            estado: newEstado,
        }
    }
    Formulario.updateOne(incidenciaToUpdate, newValues, function (err) {
        if (err) return handleError(err);
    });
};