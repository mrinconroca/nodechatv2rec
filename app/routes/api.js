var express = require("express");
router = express.Router();
var chatCntr = require(__dirname + "/../controllers/chat");
var FormCntr = require(__dirname+"/../controllers/formulario");

//Rellenar con todas las url que acepte la API
/*router.route("/chat").get(async(req,res,next)=>{
    var result = await chatCntr.load();
    res.status(200).jsonp(result);
});
*/ 

//Rutas Chat
router.route("/chat").get(async (req, res, next) => {
    var result = await chatCntr.load();
    res.render("chat", result);
});

router.route("/chat/agente").get(async (req, res, next) => {
    var result = await chatCntr.load();
    res.render("chat", { agente: true });
});

//Rutas formulario

router.route('/formulario').get(async(req,res,next)=>{
    var result = await FormCntr.load();
    console.log(result);
    res.render("formulario",result);
});

router.post('/formulario/saveCont', async (req,res) => {
    var newForm = {
        nombre: req.body.nombre,
        dni: req.body.dni,
        incidencia: req.body.incidencia,
        urgencia: req.body.urgencia,
        descripcion: req.body.descripcion,
        estado: ['Pendiente'],

    };
    FormCntr.save(newForm);
    res.redirect("/api/incidencias");
});

//Rutas incidencias

router.route('/incidencias').get(async(req,res,next)=>{
    var formRes = await FormCntr.load();
    console.log(formRes);
    res.render("incidencias",{formResults: formRes});
});

router.post('/incidencias/filterNom', async(req,res)=>{
    if(req.body.nombre == ""){
        res.redirect("/api/incidencias");
    }else{
        res.redirect("/api/incidencias/filteredNom/" + req.body.nombre);
    }
});

router.route('/incidencias/filteredNom/:nombre').get(async(req,res,next)=>{
    var formRes = await FormCntr.loadType(req.params.nombre);
    console.log(formRes);
    res.render("incidencias",{formResults: formRes});
});

router.post('/incidencias/filterEst', async(req,res)=>{
    if(req.body.estado == ""){
        res.redirect("/api/incidencias");
    }else{
        res.redirect("/api/incidencias/filteredEst/" + req.body.estado);
    }
});

router.route('/incidencias/filteredEst/:estado').get(async(req,res,next)=>{
    var formRes = await FormCntr.loadTypeEst(req.params.estado);
    console.log(formRes);
    res.render("incidencias",{formResults: formRes});
});

router.route('/incidencias/delete/:id').get(async (req, res, next) => {
    FormCntr.incidencia_delete(req.params.id);
    res.redirect("/api/incidencias");
});

//Rutas incidenciasUpdate

router.route('/incidenciasUpdate/:num').get(async(req,res,next)=>{
    var formRes = await FormCntr.load();
    console.log(formRes);
    res.render("incidenciasUpdate",{formResults: formRes, incToUp: req.params.num});
});

router.post('/incidenciasUpdate/update/:id', async (req, res) => {
    FormCntr.incidencia_update(req.params.id, req.body.incidencia, req.body.urgencia, req.body.nombre, req.body.dni, req.body.descripcion, req.body.estado);
    res.redirect("/api/incidencias");
});

module.exports = router;