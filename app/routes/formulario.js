var express = require("express");
router = express.Router();
var FormCntr = require(__dirname+"/../controllers/formulario");

router.route('/').get(async(req,res,next)=>{
    var result = await FormCntr.load();
    console.log(result);
    res.render("formulario",result);
});

router.route('/new').get((req,res,next)=>{
    var newForm = {
        nombre: 'Juan',
        dni: '12345678P',
        incidencia: [1],
        urgencia:['Alta'],
        descripcion: 'peto el ordenador'
    };
    FormCntr.save(newForm);
    res.redirect("/incidencias");
})


router.post('/saveCont', async (req,res) => {
    var newForm = {
        nombre: req.body.nombre,
        dni: req.body.dni,
        incidencia: req.body.incidencia,
        urgencia: req.body.urgencia,
        descripcion: req.body.descripcion,
    };
    FormCntr.save(newForm);
    res.redirect("/incidencias");
});

module.exports = router;