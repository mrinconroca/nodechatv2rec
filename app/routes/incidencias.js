var express = require("express");
router = express.Router();
var FormResCntr = require(__dirname+"/../controllers/formulario");


router.route('/').get(async(req,res,next)=>{
    var formRes = await FormResCntr.load();
    console.log(formRes);
    res.render("incidencias",{formResults: formRes});
});

router.post('/filter', async(req,res)=>{
    if(req.body.nombre == ""){
        res.redirect("/incidencias");
    }else{
        res.redirect("/incidencias/filtered/" + req.body.nombre);
    }
});

router.route('/filtered/:nombre').get(async(req,res,next)=>{
    var formRes = await FormResCntr.loadType(req.params.nombre);
    console.log(formRes);
    res.render("incidencias",{formResults: formRes});
});


router.route('/delete/:id').get(async (req, res, next) => {
    FormResCntr.incidencia_delete(req.params.id);
    res.redirect("/incidencias");
});

module.exports = router;