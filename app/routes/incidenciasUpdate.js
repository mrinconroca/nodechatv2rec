var express = require("express");
router = express.Router();
var FormResCntr = require(__dirname+"/../controllers/formulario");


router.route('/:num').get(async(req,res,next)=>{
    var formRes = await FormResCntr.load();
    console.log(formRes);
    res.render("incidenciasUpdate",{formResults: formRes, incToUp: req.params.num});
});

router.post('/update/:id', async (req, res) => {
    FormResCntr.incidencia_update(req.params.id, req.body.incidencia, req.body.urgencia, req.body.nombre, req.body.dni, req.body.descripcion);
    res.redirect("/incidencias");
});

module.exports = router;