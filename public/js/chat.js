$(document).ready(function () {
    const socket = io();
    $("#chat").submit(function (e) {
        e.preventDefault();
        var msg = $("#msg").val();
        $("#chatBox").append(`<p>${msg}<p>`);
        console.log($("#room").val());
        var toSend = { room: $("#room").val(), text: msg }
        socket.emit("newMSG", toSend)
    })
    socket.on("newMSG", (data) => {
        //Falta funcion de mostrar en cuadro
        //console.log(data)
        var head = ""
        if (data["room"] == "all") {
            head = "<b style='color:red'>[All]</b>"
        } else if (data["room"] == "1") {
            head = "<b style='color:red'>[Room 1]</b>"
        } else if (data["room"] == "2") {
            head = "<b style='color:red'>[Room 2]</b>"
        } else if (data["room"] == "3") {
            head = "<b style='color:red'>[Room 3]</b>"
        } else if (data["room"] == "4") {
            head = "<b style='color:red'>[Room 4]</b>"
        }
        $("#chatBox").append(`<p style="text-align: right"><b>${head}${data["user"]}:</b> ${data["text"]}<p>`);

    })
})